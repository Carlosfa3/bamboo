﻿using BambankInterfaces.Repositories;
using BambankInterfaces.Services;
using BambankModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankServices.Factories
{
    public class AccountFactory : IAccountFactory
    {
        private decimal INITIALFUNDS = 100.00m;

        public Account CreateAccount(string name, string password, int id)
        {
            var newAccount = new Account(name, password, id, INITIALFUNDS);
            return newAccount;
        }
    }
}
