﻿using BambankInterfaces.Repositories;
using BambankInterfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankServices.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IAccountRepository _accountRepository;

        public AuthenticationService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public int? GetIdByCredentials(string name, string password)
        {
            return _accountRepository.FindAccountByCredentials(name, password);   
        }
    }
}
