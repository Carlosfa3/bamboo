﻿using BambankInterfaces.Repositories;
using BambankInterfaces.Services;
using BambankModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankServices.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountFactory _accountFactory;
        private readonly IAccountRepository _accountRepository;

        public AccountService(IAccountFactory accountFactory, IAccountRepository accountRepository)
        {
            _accountFactory = accountFactory;
            _accountRepository = accountRepository;
        }

        public Account CreateAccount(string name, string password)
        {
            if(_accountRepository.FindAccountByCredentials(name, password) != null)
            {
                return null;
            }

            var id = _accountRepository.GetNumberOfAccounts() + 1;
            var acc = _accountFactory.CreateAccount(name, password, id);
            _accountRepository.AddAccount(acc);
            return acc;
        }

        public decimal Deposit(int id, decimal ammount)
        {
            Transaction deposit = new Transaction(id, id, TransactionType.Deposit, ammount);
            var acc = _accountRepository.GetAccountById(id);
            acc.Balance += ammount;
            acc.TransactionHistory.Add(deposit);
            _accountRepository.UpdateAccounts(new List<Account>() { acc });
            return acc.Balance;
        }

        public Account GetAccountById(int id)
        {
            return _accountRepository.GetAccountById(id);
        }

        public decimal Transfer(int sourceId, int desinationId, decimal ammount)
        {
            throw new NotImplementedException();
        }

        public decimal Withdraw(int id, decimal ammount)
        {
            throw new NotImplementedException();
        }
    }
}
