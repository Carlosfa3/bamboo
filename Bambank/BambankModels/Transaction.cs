﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankModels
{
    public class Transaction
    {
        public int SourceAccount { get; }
        public int DestinationAccount { get; }
        public TransactionType Type { get; }
        public decimal Ammount { get; }

        public Transaction(int source, int destination, TransactionType type, decimal ammount)
        {
            SourceAccount = source;
            DestinationAccount = destination;
            Type = type;
            Ammount = ammount;
        }
    }
}
