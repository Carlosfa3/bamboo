﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankModels
{
    public class Account
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public int Id { get; }
        public decimal Balance { get; set; }
        public List<Transaction> TransactionHistory { get; set; }

        public Account(string name, string password, int id, decimal initialBalance)
        {
            Name = name;
            Password = password;
            Id = id;
            Balance = initialBalance;
            TransactionHistory = new List<Transaction>();
        }
    }
}
