﻿using BambankExceptions;
using BambankInterfaces.Repositories;
using BambankModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankRepositories
{
    // TO-DO: Locks over these methods so multiple threads don't crash into each other
    public class AccountRepository : IAccountRepository
    {
        //Our in-memory database.
        private static readonly List<Account> Accounts = new List<Account>();

        public int? AddAccount(Account account)
        {
            //Since it would cause credentials to overlap a null is returned indicating failure
            if (Accounts.Any(acc => CompareCredentials(account.Name, account.Password, acc)))
            {
                return null;
            }

            Accounts.Add(account);
            return account.Id;
        }

        private static bool CompareCredentials(string name, string password, Account acc)
        {
            return acc.Name == name && acc.Password == password;
        }

        public int? FindAccountByCredentials(string accountName, string password)
        {
            var account = Accounts.FirstOrDefault(acc => CompareCredentials(accountName, password, acc));
            return account?.Id;
        }

        public Account GetAccountById(int id)
        {
            return Accounts.FirstOrDefault(acc => acc.Id == id);
        }

        public int GetNumberOfAccounts()
        {
            return Accounts.Count();
        }

        public void UpdateAccounts(List<Account> accounts)
        {
            //Validate Ids are valid. Iterate over list so we can capture the exact missing id
            foreach (var acc in accounts)
            {
                var index = Accounts.FindIndex(a => a.Id == acc.Id);
                if (index == -1)
                {
                    throw new BambankException($"No account with Id{acc.Id} was found!");
                }
            }
            // Update values
            foreach (var acc in accounts)
            {
                var index = Accounts.FindIndex(a => a.Id == acc.Id);
                Accounts[index] = acc;
            }
        }
    }
}
