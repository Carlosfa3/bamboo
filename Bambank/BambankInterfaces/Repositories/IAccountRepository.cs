﻿using BambankModels;
using System.Collections.Generic;

namespace BambankInterfaces.Repositories
{
    public interface IAccountRepository
    {
        Account GetAccountById(int id);
        int? AddAccount(Account account);
        int? FindAccountByCredentials(string accountName, string password);
        void UpdateAccounts(List<Account> accounts);
        int GetNumberOfAccounts();
    }
}
