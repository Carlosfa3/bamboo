﻿using BambankModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankInterfaces.Services
{
    public interface IAccountFactory
    {
        Account CreateAccount(string name, string password, int id);
    }
}
