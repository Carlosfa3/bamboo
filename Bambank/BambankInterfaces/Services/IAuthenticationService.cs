﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankInterfaces.Services
{
    public interface IAuthenticationService
    {
        int? GetIdByCredentials(string name, string password);
    }
}
