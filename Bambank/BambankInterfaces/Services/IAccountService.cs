﻿using BambankModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankInterfaces.Services
{
    public interface IAccountService
    {
        Account CreateAccount(string name, string password);
        decimal Deposit(int id, decimal ammount);
        decimal Withdraw(int id, decimal ammount);
        decimal Transfer(int sourceId, int desinationId, decimal ammount);
        Account GetAccountById(int id);
    }
}
