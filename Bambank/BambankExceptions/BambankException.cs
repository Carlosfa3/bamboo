﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BambankExceptions
{
    public class BambankException : Exception
    {
        public BambankException(string message) : base(message)
        {
        }
    }
}
