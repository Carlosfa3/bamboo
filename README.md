# Bamboo

# Technical

I've decided to use .Net Core as my language and Visual Studio community 2017 as the IDE.
I'm most accostumed to VS as an IDE so that was the most obvious choice. As for the language .Net Core is quickly growing in the industry and allows for much more abstraction and less hassle compared to classic ASP .Net.
Given the short span of time for the test I believe it will take care of more details and allow me to better demonstrate my coding structure and patterns.

# Deployment
The solution can be opened and used with VS 2017 Community or any .Net Code 2 compatible IDEs.
IIS Express is enough to launch the website.

# Projects

Interfaces:
All the Solution in interfaces will be located within this project.
Why not place the interface togethher with its classes? Because the Interface does not depend on the concrete implementeation!
An interface can be implemented by an object in another project and shouldn't be so tightly bound.
Also, by splitting up interfaces from Services and Repositories we can avoid dependencies between concrete projects moving us further away from the zone-of-pain (https://blog.ndepend.com/abstractness-entering-zone-pain/).

Services:
This will contain our Services. Controllers will utilize them for all relevant logic, serving as simple pass-through methods.

Repositories:
I've opted for simple memory storage to simplify everything.

Models:
Basic classes used solely as containers. Not to be confused with the MVC Models! These are supposed to represent DB objects.

Exceptions:
Custom exception project for easier debugging.

UnitTest:
Didn't get to create the containing folder or project within the timeframe. There would've been multiple projects, one for each existing.

# Assumptions

Accounts will be unique in id. Accounts will be unique in Name and password pairs.
Accounts should track their own transactions as well as transfers from other accounts into them.
Deleting accounts was not a goal so no methods were considered.
Cookie identification would suffice.
There were no different roles, authentication was enough.

# My Approach

First I tried to think over the methods this system should contain and designed my interfaces accordingly.
The interfaces were further updated during implementation as unforseen requirements were identified.
I took a bottom up approach to the solution, attempting to fulfil a layer before moving to the next.
Although this meant no full stack capability was in place each layer should be usable on its own into other projects and robust enough to be valuable.
My procution was less than I'd anticipated within 2 hours. Time was much tighter than I'd considered and might have tried a different approach if retaking such a challenge.
Perhaps try to get each full function in place before moving to the next?